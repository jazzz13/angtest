/**
 * Created by rinat on 20.09.14.
 */

var db = [{id:0, value:"first"}];
var id = 0;
var timeStamp = 0;

updateTimeStamp();

module.exports = function (req, res)
{
    if (req.method == "POST")
    {
        id++;

        updateTimeStamp();

        db.push({
            id: id,
            value: req.body.value
        });

        console.log(db);

        setTimeout(function ()
        {
            endResponse(res)
        }, 1500);
    }
    else if (req.method == "GET")
    {
        if (req.query.timeStamp)
        {
            var reqTimeStamp = req.query.timeStamp * 1;

            if (reqTimeStamp != timeStamp)
            {
                res.send(JSON.stringify({

                    list: db,
                    timeStamp: timeStamp
                }));
            }
        }

        res.send("{}");
    }
    else if(req.method == "DELETE")
    {
        if(req.query.id)
        {
            var deleteId = req.query.id * 1;

            for(var i in db)
            {
                var item = db[i];

                if(item.id == deleteId)
                {
                    db.splice(i, 1);

                    updateTimeStamp();

                    console.log(db);

                    break;
                }
            }
        }

        res.send("delete");
    }
};

function updateTimeStamp()
{
    timeStamp = (new Date).getTime();
}

function endResponse(res)
{
    res.end("hui");
}