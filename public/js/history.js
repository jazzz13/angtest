/**
 * Created by rinat on 21.09.14.
 */

var app = angular.module("App", []);

app.controller("History", ["$scope", "$http", function ($scope, $http)
{
    var update = false;

    var timeStamp = 0;

    setInterval(function ()
    {
        if (update)
        {
            return;
        }

        update = true;

        var params = {params: {timeStamp: timeStamp}};

        $http.get("/ajax", params)
            .success(function (r)
            {
                if (r.timeStamp)
                {
                    timeStamp = r.timeStamp;
                }

                if (r.list)
                {
                    $scope.list = r.list;
                }

                update = false;
            })
            .error(function ()
            {

                update = false;
            });

    }, 1000);

    $scope.delete = function (item)
    {
        item.disabled = true;

        var params = {params: {id: item.id + ''}};

        $http.delete("/ajax", params);
    };

}]);