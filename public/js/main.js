var app = angular.module('App', []);

app.controller("Calc", ["$scope", "$http", function ($scope, $http)
{
    $scope.value = "";

    $scope.sending = false;

    $scope.send = function ()
    {
        $scope.sending = true;

        var params = {value: $scope.value};

        $http.post("/ajax", params)
            .success(function ()
            {
                $scope.value = "";
                $scope.sending = false;
            })
            .error(function ()
            {
                alert("error =(");
                $scope.sending = false;
            });
    };

    $scope.sendAvailable = function ()
    {
        return $scope.value.length > 0 && !$scope.sending;
    };

    $scope.inputKeyPress = function (e)
    {
        if(e.keyCode == 13)
        {
            $scope.send();
        }
    };
}]);